import {
  AfterViewInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { ElementRef } from '@angular/core';
import { Component } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArraywithCoordinatesWrapper } from './model';
import { MethodsService } from './methods.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements AfterViewInit, OnDestroy {

  eventSubscription: Subscription = new Subscription();
  array: ArraywithCoordinatesWrapper = [];
  @ViewChild('canvas') canvas: ElementRef | undefined;
  private context!: CanvasRenderingContext2D;
  perimeter = 0;
  square = 0;

  constructor(
    private methodsService: MethodsService,
    private changeDetector: ChangeDetectorRef) { }

  public draw(): void {
    this.context.fillStyle = 'white';
    this.context.fillRect(0, 0, 1000, 1000);
    if (this.array.length > 0) {
      const firstDot = this.array[0];
      this.context.beginPath();
      this.context.moveTo(firstDot.x, firstDot.y);
      for (let index = 1; index < this.array.length; index++) {
        const point = this.array[index];
        this.context.lineTo(point.x, point.y);
      }
      this.context.stroke();
    }
  }

  public ngAfterViewInit(): void {
    if (this.canvas !== undefined) {
      this.context = this.canvas.nativeElement.getContext('2d');
      this.context.fillStyle = 'red';
      const canvasNE = this.canvas.nativeElement;
      const enterParams: any = prompt(
        'Would you like to enter coordinates please enter "yes", otherwise you will be able to click directly on screen?'
      );
      if (enterParams === 'yes') {
        const x: any = prompt(
          'Plese enter coordinates where 1-st number x,2-nd number y divided by ";".For example: 100,18;131,19;103,32;99,17'
        );

        const res = x.split(/\s*;\s*/);
        this.array = res.map(this.methodsService.splitToCorrectFormat);
        this.draw();
        this.perimeter = this.methodsService.calculatePerimeter(this.array);
        this.square = this.methodsService.calculateSquare(this.array);
      }

      this.eventSubscription = fromEvent(this.canvas.nativeElement, 'mousedown')
        .pipe(
          map((event: any) => {
            const rect = canvasNE.getBoundingClientRect();
            const scaleX = canvasNE.width / rect.width;
            const scaleY = canvasNE.height / rect.height;
            return {
              x: (event.clientX - rect.left) * scaleX,
              y: (event.clientY - rect.top) * scaleY,
            };
          }))
        .subscribe((pos) => {
          this.array.push(pos);
          this.draw();
          this.perimeter = this.methodsService.calculatePerimeter(this.array);
          this.square = this.methodsService.calculateSquare(this.array);
        });

      this.context.lineWidth = 0.5;
      this.context.setTransform(1, 0, 0, 1, 0.5, 0.5);
      this.changeDetector.detectChanges();
    }
  }

  ngOnDestroy(): void {
    this.eventSubscription.unsubscribe();
  }

  finish(): void {
    this.context.lineTo(this.array[0].x, this.array[0].y);
    this.context.stroke();
    this.array.push(this.array[0]);
    this.perimeter = this.methodsService.calculatePerimeter(this.array);
    this.square = this.methodsService.calculateSquare(this.array);
  }


}
