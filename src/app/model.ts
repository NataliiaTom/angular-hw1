export interface ArraywithCoordinates {
    x: number;
    y: number;
}

export interface ArraywithCoordinatesWrapper extends Array<ArraywithCoordinates> { }

