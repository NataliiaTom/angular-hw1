import { Injectable } from '@angular/core';
import { ArraywithCoordinatesWrapper } from './model';

@Injectable({
  providedIn: 'root'
})
export class MethodsService {

  constructor() { }

  public calculateSquare(arr: ArraywithCoordinatesWrapper): number {
    if (
      arr.length > 2 &&
      Math.abs(arr[arr.length - 1].x - arr[0].x) <= 1
    ) {
      let aSum = 0;
      let bSum = 0;
      let total = 0;
      for (let index = 0; index < arr.length - 1; index++) {
        const a = arr[index].x * arr[index + 1].y;
        aSum = aSum + a;
        const b = arr[index].y * arr[index + 1].x;
        bSum = bSum - b;
        total += a - b;
      }
      total = Math.floor(Math.abs(total) / 2);
      // this.square
      return total;
    } else {
      return 0;
    }
  }

  public calculatePerimeter(arr: ArraywithCoordinatesWrapper): number {
    if (
      arr.length > 2 &&
      Math.abs(arr[arr.length - 1].x - arr[0].x) <= 1
    ) {
      const result = [];
      for (let index = 1; index < arr.length; index++) {
        const a = Math.abs(arr[index].x - arr[index - 1].x);
        const b = Math.abs(arr[index].y - arr[index - 1].y);
        result.push(Math.sqrt(a * a + b * b));
      }

      return Math.floor(result.reduce((a, b) => a + b));
    } else {
      return 0;
    }
  }

  public splitToCorrectFormat(str: string): any {
    const [x, y] = str.split(/\s*,\s*/);

    return {
      x: parseInt(x, 10),
      y: parseInt(y, 10),
    };
  }
}
